import unittest

from TestingCourse.phonebook import Phonebook


class PhoneBookTest(unittest.TestCase):

    # Inherited from TestCase superclass
    def setUp(self) -> None:
        '''To remove the line duplicated in each function
        phonebook = Phonebook()
        Called each time the first.
        '''
        self.phonebook = Phonebook()

#   def tearDown(self) -> None:
#       '''Called each time after a setUp method.
#       It releases all the resources called in the setUp method or during the test.
#       It is used more when creating files of create=ing databases.
#       '''
#       pass

    def test_lookup_by_name(self):
        # phonebook = Phonebook()
        self.phonebook.add("Bob", "12345")
        number = self.phonebook.lookup("Bob")
        self.assertEqual("12345", number)

    def test_missing_name(self):
        # phonebook = Phonebook()
        with self.assertRaises(KeyError):
            self.phonebook.lookup("missing")

    # Check for consistency
    # @unittest.skip("WIP")       # To ignore testing the method.
    def test_empty_phonebook_is_consistent(self):
        # phonebook = Phonebook()
        self.assertTrue(self.phonebook.is_consistent())

    def test_is_consistent_with_different_entries(self):
        self.phonebook.add("Bob", "12345")
        self.phonebook.add("Anna", "012345")
        self.assertTrue(self.phonebook.is_consistent())

    def test_is_inconsistent_with_duplicate_entries(self):
        self.phonebook.add("Bob", "12345") # identical to Bon
        self.phonebook.add("Sue", "12345") # prefix of Bob
        self.assertFalse(self.phonebook.is_consistent())

    def test_is_inconsistent_with_duplicate_prefix(self):
        self.phonebook.add("Bob", "12345") # identical to Bon
        self.phonebook.add("Sue", "123") # prefix of Bob
        self.assertFalse(self.phonebook.is_consistent())
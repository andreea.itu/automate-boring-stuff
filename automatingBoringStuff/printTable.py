tableData = [['apples', 'oranges', 'cherries', 'banana'],
                ['Alice', 'Bob', 'Carol', 'David'],
                ['dogs', 'cats', 'moose', 'goose']]

def printTable(data):
    for j in range(len(data[0])):
        for i in range(len(data)):
            #This line finds the longest item for each list and its length
            x = len(max(data[i], key=len))
            print(data[i][j].rjust(x), end=' ')
        print()

printTable(tableData)
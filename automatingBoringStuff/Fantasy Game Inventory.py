''''Fantasy Game Inventory
You are creating a fantasy video game. The data structure to model the
player’s inventory will be a dictionary where the keys are string values
describing the item in the inventory and the value is an integer value detailing
how many of that item the player has. For example, the dictionary value
{'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12} means the
player has 1 rope, 6 torches, 42 gold coins, and so on.

Write a function named displayInventory() that would take any possible
“inventory” and display it like the following:
Inventory:
12 arrow
42 gold coin
1 rope
6 torch
1 dagger
Total number of items: 62
Hint: You can use a for loop to loop through all the keys in a dictionary.
'''

dict = {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}

def displayInventory(inventory):
    # total_items = 0
    total_items = sum(inventory.values())
    for item, quantity in inventory.items():
        print(str(quantity) + ' ' + item)
        # total_items += quantity
        # total_items = sum(inventory.keys())
    print("Total number of items: " + str(total_items))

displayInventory(dict)



'''
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
inv = {'gold coin': 42, 'rope': 1}

def addToInventory(inventory, addedItems):
    for i in range(len(addedItems)):
          inventory.setdefault(addedItems[i],0)
          inventory[addedItems[i]]=inventory[addedItems[i]]+1

inv = {'gold coin':42, 'rope':1}
dragonLoot = ['gold coin','dagger','gold coin','gold coin','ruby']
inv = addToInventory(inv, dragonLoot)
displayInventory(inv)


'''
























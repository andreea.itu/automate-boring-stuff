'''Regex Examples

1. Import the regex module with import re.
2. Create a Regex object with the re.compile() function. (Remember to use a
raw string.)
3. Pass the string you want to search into the Regex object’s search() method.
This returns a Match object.
4. Call the Match object’s group() method to return a string of the actual
matched text.

'''

import re

phoneNumRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
mo = phoneNumRegex.search('My number is 415-555-4242.')
print('Phone number found: ' + mo.group())

'''Grouping with Parentheses.
groups are created by adding parentheses'''
phoneNumRegex = re.compile(r'(\d\d\d)-(\d\d\d-\d\d\d\d)')
mo = phoneNumRegex.search('My number is 415-555-4242.')
print(mo.group(1))
print(mo.group(2))
print(mo.group(0))
print(mo.group()) # retrieve all tyhe groups at once

'''Matching Multiple Groups with the Pipe | '''
'''
Note: If you need to match an actual pipe character, escape it with a back
slash,  like \|.
'''

heroRegex = re.compile (r'Batman|Tina Fey')
mo1 = heroRegex.search('Batman and Tina Fey.')
print(mo1.group())
mo2 = heroRegex.search('Tina Fey and Batman.')
print(mo2.group())

pag 154.


'''Matching Specific Repetitions with Curly Brackets'''

haRegex = re.compile(r'(Ha){3}')
mo1 = haRegex.search('HaHaHa')
mo1.group()


'''The findall() Method

To summarize what the findall() method returns, remember the
following:
1. When called on a regex with no groups, such as \d\d\d-\d\d\d-\d\d\d\d,
the method findall() returns a list of string matches, such as ['415-555-
9999', '212-555-0000'].
2. When called on a regex that has groups, such as (\d\d\d)-(\d\d\d)-(\d\
d\d\d), the method findall() returns a list of tuples of strings (one string
for each group), such as [('415', '555', '1122'), ('212', '555', '0000')].
'''

phoneNumRegex = re.compile(r'(\d\d\d)-(\d\d\d)-(\d\d\d\d)') # has groups
phoneNumRegex.findall('Cell: 415-555-9999 Work: 212-555-0000')


'''Character classes:
By placing a caret character (^) just after the character class’s opening
bracket, you can make a negative character class. A negative character class
will match all the characters that are not in the character class'''

consonantRegex = re.compile(r'[^aeiouAEIOU]')
consonantRegex.findall('RoboCop eats baby food. BABY FOOD.')
# ['R', 'b', 'c', 'p', ' ', 't', 's', ' ', 'b', 'b', 'y', ' ', 'f', 'd', '.', '
# ', 'B', 'B', 'Y', ' ', 'F', 'D', '.']
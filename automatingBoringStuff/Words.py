'''Function to fetch words.'''

import random

WORDLIST = 'C://GIT//GitLab//BoringStuff//wordlist.txt'

def get_random_word(min_word_lenght):
    '''Get a random word from wordlist using no extra memory.'''

    num_words_process = 0
    curr_word = None
    with open(WORDLIST, 'r') as f:
        for word in f:
            if '(' in word or ')' in word:
                continue
                word = word.strip().lower()
                if len(word) < min_word_lenght:
                    continue
                num_words_process += 1
                if random.randint(1, num_words_process) == 1:
                    curr_word = word
        return curr_word
from string import ascii_lowercase
from words import get_random_word

def get_num_attempts():
    '''Get user-inputted number of incorrect attempts for the game.'''
    while True:
        num_attempts = input(
            'How many incorrect attempts do you want [1-25]' )
        try:
            num_attempts = int(num_attempts)
            if 1 <= num_attempts <= 25:
                return num_attempts
            else:
                print('{0} is not between 1 and 25'.format(num_attempts))


def get_min_word_lenght():
    '''Get user-inputted'minimum word lenght for the game.'''
    while True:
        min_word_lenght = input(
            'What minimum word lenght do you want? [4-16] ')
        try:
            min_word_lenght = int(min_word_lenght)
            if 4 <= min_word_lenght <= 16:

def printPicnic(itemsDict, leftWidth, rightWidth):
    print('PICNIC ITEMS'.center(leftWidth + rightWidth, '*'))
    for v, k in itemsDict.items():
        print(v.ljust(leftWidth, '.') + str(k).rjust(rightWidth))


picnicItems = {'sandwiches': 4, 'apples': 12, 'cups': 4, 'cookies': 8000}

printPicnic(picnicItems, 12, 5)
printPicnic(picnicItems, 20, 6)


def printPicnic2(itemsDict, leftWidth, rightWidth):
    print('PICNIC ITEMS'.center(leftWidth + rightWidth, '#'))
    for v, k in itemsDict.items():
        print(v.ljust(leftWidth, '*') + str(k).rjust(rightWidth))


picnicItems = {'sandwiches': 4, 'apples': 12, 'cups': 4, 'cookies': 8000}

printPicnic2(picnicItems, 12, 5)
printPicnic2(picnicItems, 20, 6)
